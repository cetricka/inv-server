exports.isJsonEmpty = (obj) => {
  for(var key in obj) {
      if(obj.hasOwnProperty(key))
          return false;
  }
  return true;
}

exports.colData = (obj) => {
  let data = []
  for(var key in obj) {
      data.push(key)
  }
  return data;
}