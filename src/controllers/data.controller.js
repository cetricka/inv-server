'use strict';

const Data = require('../models/data.model');
const { colData } = require('../../util/validators');

exports.findAll = function(req,res) {
  Data.findAll(function(err, data) {
    if (err){
      res.send(err);
    } else {
      res.send(data);
    }
  });
};

exports.findColumns = function(req,res) {
  Data.findColumns(function(err, data) {
    if (err){
      res.send(err);
    } else {
      var resultArray = Object.values(JSON.parse(JSON.stringify(data)));
      res.send(colData(resultArray[0]));
    }
  });
};