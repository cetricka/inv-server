'use strict';

var dbConn = require('../../config/db.config');
const { isJsonEmpty } = require('../../util/validators');

//Data object create
var Data = function(data){
  this.budget = data.budget;
  this.genres = data.genres;
  this.homepage = data.homepage;
  this.id = data.id;
  this.keywords = data.keywords;
  this.original_language = data.original_language;
  this.original_title = data.original_title;
  this.overview = data.overview;
  this.popularity = data.popularity;
  this.production_companies = data.production_companies;
  this.production_countries = data.production_countries;
  this.release_date = data.release_date;
  this.revenue = data.revenue;
  this.runtime = data.runtime;
  this.spoken_languages = data.spoken_languages;
  this.status = data.status;
  this.tagline = data.tagline;
  this.title = data.title;
  this.vote_average = data.vote_average;
  this.vote_count = data.vote_count;
};

var errorMessage = { error:true };


Data.findColumns = function (result) {
  dbConn.query("SELECT `budget`, `genres`, `homepage`, `id`, `keywords`, `original_language`, `original_title`, `overview`, `popularity`, `production_companies`, `production_countries`, `release_date`, `revenue`, `runtime`, `spoken_languages`, `status`, `tagline`, `title`, `vote_average`, `vote_count` FROM `movies` LIMIT 0,1", function (err, res) {
    if(err) {
      result(null, errorMessage);
    } else{
      if (isJsonEmpty(res)){
        result(null, errorMessage);
      } else {
        result(null, res);
      }
    }
  });
};

Data.findAll = function (result) {
  dbConn.query("SELECT `budget`, `genres`, `homepage`, `id`, `keywords`, `original_language`, `original_title`, `overview`, `popularity`, `production_companies`, `production_countries`, `release_date`, `revenue`, `runtime`, `spoken_languages`, `status`, `tagline`, `title`, `vote_average`, `vote_count` FROM `movies` LIMIT 0,100", function (err, res) {
    if(err) {
      result(null, errorMessage);
    } else{
      if (isJsonEmpty(res)){
        result(null, errorMessage);
      } else {
        result(null, res);
      }
    }
  });
};

module.exports= Data;