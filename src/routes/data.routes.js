const express = require('express')

const router = express.Router()

const dataController =   require('../controllers/data.controller');

// Retrieve all data
router.get('/', dataController.findAll);

// Retrieve a single event with id
router.get('/columns', dataController.findColumns);

module.exports = router