const express = require('express')
const app = express()
const env = require('dotenv').config();
var cors = require('cors');

const dataRoutes = require('./src/routes/data.routes')

const { SERVER_PORT } = process.env;

app.use(express.json())
app.use(cors())

//Root route
app.get('/', (req, res) => {
  res.send("Movie data API. > ");
});

//data routes
app.use('/data', dataRoutes)

//404's
app.get('*', function(req, res){
  res.status(404);

  if (req.accepts('json')) {
    res.send("Whoops!");
    return;
  }

  res.type('txt').send('Not found');
});

// Setup server port
const port = SERVER_PORT;
// listen for requests
app.listen(port, () => {
  console.log(`server started on port ${port}`)
});